<?php
/**
 * Created by PhpStorm.
 * User: fernando
 * Date: 12-6-17
 * Time: 0:38
 */

namespace AppBundle\Service;

class GameBoard
{
    private $board;

    /**
     * @param int $grid
     *
     * Creates a new Game board of $grid by $grid by default 3 by 3
     */
    public function create($grid = 3)
    {
        $board = array_fill(0, $grid, array_fill(0, $grid, ''));

        $this->setBoard($board);
    }

    /**
     * @param $xPosition
     * @param $yPosition
     * @param $playerMarker
     *
     * places a move independently of being the player or the bot playing
     */
    public function placeMove($xPosition, $yPosition, $playerMarker)
    {
        $board = $this->getBoard();
        $board[$yPosition][$xPosition] = $playerMarker;
        $this->setBoard($board);
    }

    /**
     * @param $xPosition
     * @param $yPosition
     * @return bool
     *
     * Validates if the move is valid before placing it
     */
    public function validateMove($xPosition, $yPosition)
    {
        if ($this->getBoard()[$yPosition][$xPosition] === '')
        {
            return true;
        }

        return false;
    }

    /**
     * @return bool|mixed
     *
     * Analizes the board to figure out if we have a winner
     */
    public function validateBoard()
    {

        $result = $this->checkLines();
        if ($result !== false)
        {
            return $result;
        }

        $result = $this->checkColumns();
        if ($result !== false)
        {
            return $result;
        }

        $result = $this->checkDiagonals();
        if ($result !== false)
        {
            return $result;
        }

        return false;
    }

    /**
     * @return bool|mixed
     *
     * Checks Lines for 3 in a row
     */
    public function checkLines()
    {
        foreach ($this->getBoard() as $line)
        {
            $result = $this->checkLine($line);
            if ($result !== false)
            {
                return $result;
            }
        }

        return false;
    }

    /**
     * @return bool|mixed
     *
     * Checks Columns for 3 in a row
     */
    public function checkColumns()
    {
        for ($i=0; $i < 3; $i++)
        {
            $values = [];

            for ($z=0; $z < 3; $z++)
            {
                $values[] = $this->getBoard()[$z][$i];
            }

            $result = $this->checkLine($values);
            if ($result !== false)
            {
                return $result;
            }
        }

        return false;
    }

    /**
     * @return bool|mixed
     *
     * Checks Diagonals for 3 in a row
     */
    public function checkDiagonals()
    {
        $board = $this->getBoard();

        $positions = [
            [0,0],
            [1,1],
            [2,2]
        ];

        for ($z=0; $z < 2; $z++){

            $values = [];
            foreach ($positions as $position)
            {
                $values[] = $board[$position[0]][$position[1]];
            }

            $result = $this->checkLine($values);
            if ($result !== false)
            {
                return $result;
            }

            $board = array_reverse($board);
        }

        return false;
    }

    /**
     * @param $line
     * @return bool|mixed
     *
     * Gets an array of 3 board positions checks if they are
     * the same character defining the winner
     */
    private function checkLine($line)
    {
        $result = array_unique($line);
        if (!in_array('', $line) && count($result) == 1)
        {
            return current($result);
        }

        return false;
    }

    /**
     * @return array
     *
     * Gets all the possible moves in the board
     */
    public function getValidMoves()
    {
        $validMoves = [];
        $rowIterator = 0;
        foreach ($this->getBoard() as $row)
        {
            $columnIterator = 0;
            foreach ($row as $column)
            {
                if ($column == '')
                {
                    $validMoves[] = [
                        'bot_position_x' => $columnIterator,
                        'bot_position_y' => $rowIterator
                    ];
                }
                $columnIterator++;
            }
            $rowIterator++;
        }

        return $validMoves;
    }

    /**
     * @return int
     *
     * counts the number of possible moves
     */
    public function countValidMoves()
    {
        return count($this->getValidMoves());
    }

    /**
     * @return mixed
     */
    public function getBoard ()
    {
        return $this->board;
    }

    /**
     * @param mixed $board
     */
    public function setBoard ($board)
    {
        $this->board = $board;
    }
}