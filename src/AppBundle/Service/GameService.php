<?php
/**
 * Created by PhpStorm.
 * User: fernando
 * Date: 15-6-17
 * Time: 4:06
 */

namespace AppBundle\Service;

class GameService
{
    private $gameBoardService;
    private $derpBotService;
    private $isGameOver;
    private $isWinner;

    private $playerMark;
    private $botMark;
    private $playerGoesFirst;

    function __construct (GameBoard $gameBoard, DerpBot $derpBot)
    {
        $this->gameBoardService = $gameBoard;
        $this->derpBotService = $derpBot;
    }

    /**
     * @param int $grid
     *
     * Creates a new game
     */
    public function newGame($grid = 3)
    {
        $playerGoesFirst = false;
        if (rand(0, 1))
        {
            $playerGoesFirst = true;
        }

        $this->setPlayerMark($playerGoesFirst ? 'X' : 'O');
        $this->setBotMark($playerGoesFirst ? 'O' : 'X');
        $this->derpBotService->setMarker($playerGoesFirst ? 'O' : 'X');

        $this->setPlayerGoesFirst($playerGoesFirst);
        $this->gameBoardService->create($grid);
        $this->setIsGameOver(false);
    }

    /**
     * @return array
     *
     * Executes a game play by the DerpBot
     */
    public function derpBotPlays()
    {
        $botplay = $this->derpBotService->play($this->gameBoardService->getBoard());

        $this->gameBoardService->placeMove(
            $botplay['bot_position_x'],
            $botplay['bot_position_y'],
            $this->getBotMark()
        );

        $this->checkGameStatus();

        return $botplay;
    }

    /**
     * @param $playerMoveX
     * @param $playerMoveY
     * @return bool
     *
     * Executes a player move
     */
    public function playerMove($playerMoveX, $playerMoveY)
    {
        if($this->gameBoardService->validateMove(
            $playerMoveX,
            $playerMoveY) &&
            !$this->getIsGameOver())
        {
            $this->gameBoardService->placeMove(
                $playerMoveX,
                $playerMoveY,
                $this->getPlayerMark()
            );

            $this->checkGameStatus();

            return true;
        }

        return false;
    }

    /**
     * Checks the game status, if the game is over and
     * if there is a winner
     */
    public function checkGameStatus()
    {
        $result = $this->gameBoardService->validateBoard();

        if ($result !== false)
        {
            $this->setIsGameOver(true);
            $this->setIsWinner($result);
        }

        if ($this->gameBoardService->countValidMoves() == 0 && !$this->getIsGameOver())
        {
            $this->setIsGameOver(true);
            $this->setIsWinner('draw');
        }
    }

    /**
     * @return mixed
     */
    public function getPlayerGoesFirst ()
    {
        return $this->playerGoesFirst;
    }

    /**
     * @param mixed $playerGoesFirst
     */
    public function setPlayerGoesFirst ($playerGoesFirst)
    {
        $this->playerGoesFirst = $playerGoesFirst;
    }

    /**
     * @return mixed
     */
    public function getBoard ()
    {
        return $this->board;
    }

    /**
     * @param mixed $board
     */
    public function setBoard ($board)
    {
        $this->board = $board;
    }

    /**
     * @return mixed
     */
    public function getPlayerMark ()
    {
        return $this->playerMark;
    }

    /**
     * @param mixed $playerMark
     */
    public function setPlayerMark ($playerMark)
    {
        $this->playerMark = $playerMark;
    }

    /**
     * @return mixed
     */
    public function getBotMark ()
    {
        return $this->botMark;
    }

    /**
     * @param mixed $botMark
     */
    public function setBotMark ($botMark)
    {
        $this->botMark = $botMark;
    }

    /**
     * @return mixed
     */
    public function getIsGameOver ()
    {
        return $this->isGameOver;
    }

    /**
     * @param mixed $isGameOver
     */
    public function setIsGameOver ($isGameOver)
    {
        $this->isGameOver = $isGameOver;
    }

    /**
     * @return mixed
     */
    public function getIsWinner ()
    {
        return $this->isWinner;
    }

    /**
     * @param mixed $isWinner
     */
    public function setIsWinner ($isWinner)
    {
        $this->isWinner = $isWinner;
    }

    /**
     * @return GameBoard
     */
    public function getGameBoardService ()
    {
        return $this->gameBoardService;
    }
}