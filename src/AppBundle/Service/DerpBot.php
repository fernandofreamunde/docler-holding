<?php
/**
 * Created by PhpStorm.
 * User: fernando
 * Date: 14-6-17
 * Time: 1:18
 */

namespace AppBundle\Service;


class DerpBot
{
    private $gameBoard;
    private $marker;

    function __construct (GameBoard $gameBoard)
    {
        $this->gameBoard = $gameBoard;
    }

    /**
     * @return array
     * 
     * Bot decides where to place its move
     */
    public function play()
    {
        $validMoves = $this->gameBoard->getValidMoves();

        $randomMoveKey = 0;
        if (count($validMoves) > 1)
        {
            $randomMoveKey = array_rand($validMoves, 1);
        }

        return [
            'bot_position_x' => $validMoves[$randomMoveKey]['bot_position_x'],
            'bot_position_y' => $validMoves[$randomMoveKey]['bot_position_y'],
        ];
    }

    public function setMarker($marker)
    {
        $this->marker = $marker;
    }

    public function getMarker()
    {
        return $this->marker;
    }
}