<?php

namespace AppBundle\Controller;

use AppBundle\Service\GameService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\JsonResponse;

class GameController extends Controller
{
    private $session;

    function __construct ()
    {
        $this->session = new Session();
    }

    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        $canContinue = true;

        if (!$this->session->get('game'))
        {
            $canContinue = false;
        }

        return $this->render('default/index.html.twig', ['continue' => $canContinue]);
    }

    /**
     * @Route("/board/{continue}", name="board")
     */
    public function boardAction($continue)
    {
        /** @var $gameService GameService*/
        $gameService = $this->container->get('app.service.gameservice');

        if (!$continue)
        {
            $gameService->newGame();

            if (!$gameService->getPlayerGoesFirst())
            {
                $gameService->derpBotPlays();
            }

            $this->session->set('game', $gameService);
        }

        $gameService = $this->session->get('game');

        return $this->render('default/board.html.twig',
            [
                'board'        => $gameService->getGameBoardService()->getBoard(),
                'player'       => $gameService->getPlayerMark(),
                'bot'          => $gameService->getBotMark(),
                'player_first' => $gameService->getPlayerGoesFirst(),
            ]);
    }

    /**
     * @Route("/play", name="play")
     */
    public function playAction(Request $request)
    {
        $playerMoveX    = $request->request->get('xposition');
        $playerMoveY    = $request->request->get('yposition');

        /** @var $gameService GameService*/
        $gameService = $this->session->get('game');

        if(!$gameService->playerMove(
            $playerMoveX,
            $playerMoveY)
        )
        {
            $response = [
                'board' => json_encode($gameService->getGameBoardService()->getBoard()),
                'move' => 'invalid',
                'move_position_x' => $playerMoveX,
                'move_position_y' => $playerMoveY
            ];

            return new JsonResponse($response);
        }

        if($gameService->getIsGameOver())
        {
            $response = [
                'move' => 'valid',
                'board' => json_encode($gameService->getGameBoardService()->getBoard()),
                'winner' => $gameService->getIsWinner(),
                'game' => 'over'
            ];

            return new JsonResponse($response);
        }

        $botplay = $gameService->derpBotPlays();
        $this->session->set('game', $gameService);

        if($gameService->getIsGameOver())
        {
            $response = [
                'move' => 'valid',
                'board' => json_encode($gameService->getGameBoardService()->getBoard()),
                'winner' => $gameService->getIsWinner(),
                'bot_position_x' => $botplay['bot_position_x'],
                'bot_position_y' => $botplay['bot_position_y'],
                'game' => 'over'
            ];

            return new JsonResponse($response);
        }

        return new JsonResponse([
            'move' => 'valid',
            'board' => json_encode($gameService->getGameBoardService()->getBoard()),
            'bot_position_x' => $botplay['bot_position_x'],
            'bot_position_y' => $botplay['bot_position_y']
        ]);
    }

    /**
     * @Route("/session", name="session")
     */
    public function sessionAction()
    {
        $this->session->clear();

        return $this->redirectToRoute('homepage');
    }
}
