<?php
/**
 * Created by PhpStorm.
 * User: fernando
 * Date: 15-6-17
 * Time: 23:50
 */

namespace tests\AppBundle;


use AppBundle\Service\DerpBot;
use AppBundle\Service\GameBoard;
use AppBundle\Service\GameService;
use PHPUnit\Framework\TestCase;

class GameServiceTest extends TestCase
{
    public function testNewGame()
    {
        $gameBoardService = new GameBoard();
        $derpBotService   = new DerpBot($gameBoardService);
        $gameService = new GameService($gameBoardService, $derpBotService);

        $gameService->newGame();
        $this->assertFalse($gameService->getIsGameOver());
        $this->assertTrue(in_array($gameService->getPlayerMark(), ['X','O']));
        $this->assertTrue(in_array($gameService->getBotMark(), ['X','O']));
    }

    public function testDerpBotPlays()
    {
        $gameBoardService = new GameBoard();
        $derpBotService   = new DerpBot($gameBoardService);
        $gameService = new GameService($gameBoardService, $derpBotService);

        $gameService->newGame();
        $result = $gameService->derpBotPlays();
        $this->assertEquals(2, count($result));
    }

    public function testPlayerMoveIsValid()
    {
        $gameBoardService = new GameBoard();
        $derpBotService   = new DerpBot($gameBoardService);
        $gameService = new GameService($gameBoardService, $derpBotService);

        $gameService->newGame();
        $this->assertTrue($gameService->playerMove(0,0));
    }

    public function testPlayerMoveIsInalid()
    {
        $gameBoardService = new GameBoard();
        $derpBotService   = new DerpBot($gameBoardService);
        $gameService = new GameService($gameBoardService, $derpBotService);

        $gameService->newGame();
        $gameService->playerMove(0,0);
        $this->assertFalse($gameService->playerMove(0,0));
    }

    public function testCheckGameStatusIsGameNotOver()
    {
        $gameBoardService = new GameBoard();
        $derpBotService   = new DerpBot($gameBoardService);
        $gameService = new GameService($gameBoardService, $derpBotService);

        $gameService->newGame();
        $gameService->checkGameStatus();
        $this->assertFalse($gameService->getIsGameOver());
        $this->assertNull($gameService->getIsWinner());
    }

    public function testCheckGameStatusIsGameOverDraw()
    {
        $gameBoardService = new GameBoard();
        $derpBotService   = new DerpBot($gameBoardService);
        $gameService = new GameService($gameBoardService, $derpBotService);

        $gameService->newGame();
        $gameService->getGameBoardService()->placeMove(0,0,'X');
        $gameService->getGameBoardService()->placeMove(0,1,'X');
        $gameService->getGameBoardService()->placeMove(0,2,'O');
        $gameService->getGameBoardService()->placeMove(1,0,'O');
        $gameService->getGameBoardService()->placeMove(1,1,'O');
        $gameService->getGameBoardService()->placeMove(1,2,'X');
        $gameService->getGameBoardService()->placeMove(2,0,'X');
        $gameService->getGameBoardService()->placeMove(2,1,'O');
        $gameService->getGameBoardService()->placeMove(2,2,'X');
        $gameService->checkGameStatus();
        $this->assertTrue($gameService->getIsGameOver());
        $this->assertEquals('draw', $gameService->getIsWinner());
    }

    public function testCheckGameStatusIsGameOverXwins()
    {
        $gameBoardService = new GameBoard();
        $derpBotService   = new DerpBot($gameBoardService);
        $gameService = new GameService($gameBoardService, $derpBotService);

        $gameService->newGame();
        $gameService->getGameBoardService()->placeMove(0,0,'X');
        $gameService->getGameBoardService()->placeMove(0,1,'X');
        $gameService->getGameBoardService()->placeMove(0,2,'X');
        $gameService->getGameBoardService()->placeMove(1,0,'O');
        $gameService->getGameBoardService()->placeMove(1,1,'O');
        $gameService->getGameBoardService()->placeMove(1,2,'X');
        $gameService->getGameBoardService()->placeMove(2,1,'O');
        $gameService->getGameBoardService()->placeMove(2,2,'X');
        $gameService->checkGameStatus();
        $this->assertTrue($gameService->getIsGameOver());
        $this->assertEquals('X', $gameService->getIsWinner());
    }
}