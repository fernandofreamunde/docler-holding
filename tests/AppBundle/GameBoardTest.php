<?php
/**
 * Created by PhpStorm.
 * User: fernando
 * Date: 15-6-17
 * Time: 16:04
 */

namespace tests\AppBundle;


use AppBundle\Service\GameBoard;
use PHPUnit\Framework\TestCase;

class GameBoardTest extends TestCase
{
    public function testCreate()
    {
        $board = new GameBoard();
        $board->create();

        $this->assertEquals(3, count($board->getBoard()));
        $this->assertEquals(3, count($board->getBoard()[0]));
    }

    public function testCreateGrid5x5()
    {
        $board = new GameBoard();
        $board->create(5);

        $this->assertEquals(5, count($board->getBoard()));
        $this->assertEquals(5, count($board->getBoard()[0]));
    }

    public function testCreateGrid10x10()
    {
        $board = new GameBoard();
        $board->create(10);

        $this->assertEquals(10, count($board->getBoard()));
        $this->assertEquals(10, count($board->getBoard()[0]));
    }

    public function testValidateMove()
    {
        $board = new GameBoard();
        $board->create();

        $this->assertTrue($board->validateMove(1,1));
    }

    public function testValidateInvalidMove()
    {
        $board = new GameBoard();
        $board->create();

        $board->placeMove(1,1, 'X');
        $this->assertFalse($board->validateMove(1,1));
    }

    public function testPlaceMove()
    {
        $board = new GameBoard();
        $board->create();

        $board->placeMove(1,1, 'X');
        $this->assertEquals('X', $board->getBoard()[1][1]);
    }

    public function testValidateEmptyBoard()
    {
        $board = new GameBoard();
        $board->create();

        $this->assertFalse($board->validateBoard());
    }

    public function testValidateBoardWithAColumnOfX()
    {
        $board = new GameBoard();
        $board->create();

        $board->placeMove(0,0, 'X');
        $board->placeMove(0,1, 'X');
        $board->placeMove(0,2, 'X');
        $this->assertNotFalse($board->validateBoard());
        $this->assertEquals('X',$board->validateBoard());
    }

    public function testValidateBoardWithALineOfO()
    {
        $board = new GameBoard();
        $board->create();

        $board->placeMove(0,0, 'O');
        $board->placeMove(1,0, 'O');
        $board->placeMove(2,0, 'O');
        $this->assertNotFalse($board->validateBoard());
        $this->assertEquals('O',$board->validateBoard());
    }

    public function testValidateBoardWithADiagonalOfX()
    {
        $board = new GameBoard();
        $board->create();

        $board->placeMove(0,0, 'X');
        $board->placeMove(1,1, 'X');
        $board->placeMove(2,2, 'X');
        $this->assertNotFalse($board->validateBoard());
        $this->assertEquals('X',$board->validateBoard());
    }

    public function testValidateBoardWithAReverseDiagonalOfO()
    {
        $board = new GameBoard();
        $board->create();

        $board->placeMove(0,2, 'O');
        $board->placeMove(1,1, 'O');
        $board->placeMove(2,0, 'O');
        $this->assertNotFalse($board->validateBoard());
        $this->assertEquals('O',$board->validateBoard());
    }

    public function testGetValidMoves()
    {
        $board = new GameBoard();
        $board->create();

        $board->placeMove(0,2, 'X');
        $board->placeMove(1,1, 'O');
        $board->placeMove(2,0, 'X');

        $this->assertEquals( 6, count($board->getValidMoves()));

        foreach ($board->getValidMoves() as $move)
        {
            $this->assertTrue($board->validateMove($move['bot_position_x'],$move['bot_position_y']));
        }
    }

    public function testCountValidMoves()
    {
        $board = new GameBoard();
        $board->create();

        $board->placeMove(0,2, 'X');
        $board->placeMove(1,1, 'O');
        $board->placeMove(2,0, 'X');
        $board->placeMove(0,0, 'O');

        $this->assertEquals( 5, $board->countValidMoves());
    }
}