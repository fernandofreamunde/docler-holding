## Tic-Tac-Toe

This project was initiated as an assessment for a Backend position at [Docler Holding](www.doclerholding.com).

The task in hand is to implement a classic Tic-Tac-Toe game.

The solution should be a simple web application with an interface where the users can play the game and an API which this interface communicates with.

### Requirements

 - [x] 3 x 3 grid.
 - [x] Users should be play against a bot.
 - [x] There should be some form of indication when the game ended, and this should contain which user won.
 - [x] Optional - Support games with N x N grid.
 - [ ] Optional - Clever bot, which doesn’t let the user win.

## Rules

- Business logic must be implemented on the backend.
- The backend must be written in PHP (any PHP version).
- Feel free to use any backend framework.
- If you use database, it’s up to you what kind.
- For the frontend you can use any JavaScript dialect XHTML, HTML5, etc...
- Easy installation/setup (use docker or vagrant if necessary).
- (Optional) Unit tests for the backend.

## Instalation

If every dependency is met, all that is needed to do in order to execute this game is to open a terminal and run from the root dir:
`php /bin/console server:run`

it will give you an url, just click it.

## Dependencies

- have a system with php 7 installed (it probably also works on 5.6 but I use 7 on my system)
- be connected to the Internet (jQuery is fetched from the Internet)

## How to use

After arriving to the home page of the project you can initiate a new game.

When the game is over you get a message stating that the game is over and a little word from your adversary, he doesn't like to loose.
You can start a new Game from that message.

If for some reason you wnat to kill your session just visit the `/session` endpoint.
